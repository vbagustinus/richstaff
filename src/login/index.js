import React, { Component } from 'react';
import { Text, Button, AsyncStorage, Image } from 'react-native';
import A from 'axios'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin'
import { API } from '../api'
import { View, TouchableOpacity } from '../components'
import { Logo } from '../assets';

GoogleSignin.configure({
  // scopes: ['https://www.googleapis.com/auth/userinfo.email'], // what API you want to access on behalf of the user, default is email and profile
  webClientId: '824333562625-go4l2p775roe4usn49ttus5faqfi2ei4.apps.googleusercontent.com',
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: 'investree.id', // specifies a hosted domain restriction
//   // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//   forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login. 
})

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      error: null,
    };
    this._signIn = this._signIn.bind(this)
    this.signOut = this.signOut.bind(this)
  }

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

  // Somewhere in your code
  _signIn = async () => {
    const { navigation: { navigate } } = this.props
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log('API', `${API}auth/register`)
      A.post(`${API}auth/register`, userInfo.user)
      .then( async ({ data }) => {
        await AsyncStorage.setItem('user', JSON.stringify(data.data))
        .then(() => {
          navigate('App')
        })
      })
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        console.log('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        console.log('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('play services not available or outdated');
      } else {
        console.log('Something went wrong', error.toString());
        this.setState({
          error,
        });
      }
    }
  };


  render() {
    const { userInfo, error } = this.state
    return (
      <View
        center
        style={{
          backgroundColor: 'rgb(199,151,201)',
          paddingTop: 100,
        }}
      >
        <Image
          style={{
            width: 200,
            height: 300,
            margin: 50,
            marginBottom: 0,
            shadowColor: '#000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 1,
            marginLeft: 5,
            marginRight: 5,
            marginBottom: 70,
          }}
          source={Logo}
        />
        <TouchableOpacity
          centering
          unflex
          style={{ width: 212, height: 48, backgroundColor: 'rgb(153,62,155)' }}
          onPress={this._signIn}
        >
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              color: '#f0f0f0',
            }}
          >
            Masuk dengan Google
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
