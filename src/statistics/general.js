import React, { Component } from 'react';
import { Text, processColor, FlatList, ScrollView } from 'react-native';
import { PieChart } from 'react-native-charts-wrapper'
import A from 'axios'
import { API } from '../api'
import { View } from '../components'

export default class General extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation
    this.state = {
      dataTopRanks: [],
      dataUsers: [],
      legend: {
        enabled: true,
        textSize: 15,
        form: 'CIRCLE',

        horizontalAlignment: "RIGHT",
        verticalAlignment: "CENTER",
        orientation: "VERTICAL",
        wordWrapEnabled: true
      },
      data: {
        dataSets: [{
          values: state ? state.params.general : [
            {value: 0, label: ''},
            {value: 0, label: ''},
            {value: 0, label: ''},
            {value: 0, label: ''},
            {value: 0, label: ''}],
          label: '',
          config: {
            colors: [processColor('#C0FF8C'), processColor('#FFF78C'), processColor('#FFD08C'), processColor('#8CEAFF'), processColor('#FF8C9D')],
            valueTextSize: 20,
            valueTextColor: processColor('green'),
            sliceSpace: 5,
            selectionShift: 13,
            valueFormatter: "#.#'%'",
            valueLineColor: processColor('green'),
            valueLinePart1Length: 0.5
          }
        }],
      },
      highlights: [{x:2}],
      description: {
        text: '',
        textSize: 1,
        textColor: processColor('rgb(199,151,201)'),

      }
    };
  }

  componentDidMount() {
    A.get('https://jsonplaceholder.typicode.com/users')
    .then(({ data }) => {
      this.setState({ dataUsers: data })
    })
    .catch(err => {
      console.log(err)
    })
  }

  _keyExtractor = (item, index) => item.id;

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null})
    } else {
      this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
    }

    console.log(event.nativeEvent)
  }

  _renderItem = ({item}) => (
    <View
      unflex
      style={{
        height: 60,
        backgroundColor: 'rgb(153,62,155)',
        padding: 10,
        paddingLeft: 30,
      }}
    >
      <Text
        style={{
          fontSize: 15,
          color: '#f0f0f0',
          fontWeight: 'bold',
          marginBottom: 0,
        }}
      >{item.name}</Text>
    </View>
  );

  render() {
    const { dataUsers, data } = this.state
    console.log('DATA', data)
    return (
      <View
        column
        style={{
          backgroundColor: 'rgb(199,151,201)',
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            fontSize: 20,
            color: '#f0f0f0',
            margin: 30,
            fontWeight: 'bold',
            marginBottom: 0,
          }}
        >
          Rich Clasemen
        </Text>
        <View
          unflex
          style={{
            height: 350,
          }}
        >
          <PieChart
            style={{ flex: 1}}
            logEnabled={true}
            chartBackgroundColor={processColor('rgb(199,151,201)')}
            chartDescription={this.state.description}
            data={this.state.data}
            legend={this.state.legend}
            highlights={this.state.highlights}

            entryLabelColor={processColor('green')}
            entryLabelTextSize={20}
            drawEntryLabels={true}

            rotationEnabled={true}
            rotationAngle={45}
            usePercentValues={true}
            styledCenterText={{text:'Top 5', color: processColor('rgb(199,151,201)'), size: 30, fontWeight: 'bold',}}
            centerTextRadiusPercent={100}
            holeRadius={40}
            holeColor={processColor('#f0f0f0')}
            transparentCircleRadius={45}
            transparentCircleColor={processColor('#f0f0f088')}
            maxAngle={350}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
        <View style={{ backgroundColor: 'rgb(153,62,155)' }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 20,
              color: '#f0f0f0',
              margin: 30,
              fontWeight: 'bold',
              marginVertical: 10,
            }}
          >
            Winner History
          </Text>
          <ScrollView>
            <FlatList
              data={dataUsers}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}
