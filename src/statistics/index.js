import React, { Component } from 'react';
import { Text, Dimensions, AsyncStorage } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import A from 'axios'
import { API } from '../api'
import { View, TouchableOpacity } from '../components'

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')

export default class Statistics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      general: null,
    };
  }

  async componentDidMount() {
    const { navigation } = this.props
    try {
      const value = await AsyncStorage.getItem('user');
      if (value !== null) {
        // We have data!!
        const user = JSON.parse(value)
        console.log(user);
        A.get(`${API}dashboard/details/${user && user.accessToken}`)
        .then(({ data }) => {
          this.setState({ specific: data.data })
        })
        .catch(err => {
          console.log(err)
        })
      } else {
        await AsyncStorage.removeItem('user')
        .then(async () => {
          const navigateAction = NavigationActions.navigate({
            routeName: 'AppNavigator',
            action: NavigationActions.navigate({ routeName: 'Login' }),
          });
          this.props.navigation.dispatch(navigateAction);
          console.log('MASUK KOSONG')
        })
      }
    } catch (error) {
      // Error retrieving data
    }
    A.get(`${API}dashboard/ranks/`)
    .then(({ data }) => {
      let arrayTop = []
      data.data.map(item => {
        let obj = {
          value: item.point,
          label: item.name,
        }
        arrayTop.push(obj)
      })
      this.setState({ general: arrayTop })
    })
    .catch(err => {
      console.log(err)
    })
  }

  render() {
    const { navigation: { navigate } } = this.props
    const { general, specific } = this.state
    return (
      <View centering column>
        <TouchableOpacity
          onPress={() => navigate('General', { general })}
          unflex
          centering
          style={{
            width: WIDTH - 100,
            height: WIDTH - 100,
            backgroundColor: 'rgb(199,151,201)',
            borderRadius: 30,
            marginBottom: 20,
            }}
          >
          <Ionicons name={'ios-analytics'} size={150} color={'#fff'} />
          <Text
            style={{
              color: '#fff',
              fontSize: 40,
            }}
          > Rich General </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigate('Specific', { specific })}
          unflex
          centering
          style={{
            width: WIDTH - 100,
            height: WIDTH - 100,
            backgroundColor: 'rgb(199,151,201)',
            borderRadius: 30,
            }}
          >
          <Ionicons name={'ios-cash'} size={150} color={'#fff'} />
          <Text
            style={{
              color: '#fff',
              fontSize: 40,
            }}
          > Rich Me </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
