import React, { Component } from 'react';
import { Text, processColor, Dimensions } from 'react-native';
import {BarChart} from 'react-native-charts-wrapper'
import { View } from '../components'

const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')

export default class Specific extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation
    const { params:  { specific } } = state
    this.state = {
      legend: {
        enabled: true,
        textSize: 14,
        form: "SQUARE",
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        wordWrapEnabled: true
      },
      data: {
        dataSets: [{
          values: [{y:[specific && specific.step.step1, 0, 0, 0], marker: ['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4']}, {y:[0, specific && specific.step.step2, 0, 0], marker: ['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4']}, {y:[0, 0, specific && specific.step.step3, 0], marker:['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4']}, {y:[0, 0, 0, specific && specific.step.step4], marker: ['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4']}],
          label: '',
          config: {
            colors: [processColor('#C0FF8C'), processColor('#FFF78C'), processColor('#FFD08C'), processColor('#8CEAFF')],
            stackLabels: ['Invitation', 'Completing', 'Docusign', 'Funding']
          }
        }],
      },
      highlights: [{x: 1, stackIndex: 2}, {x: 2, stackIndex: 1}],
      xAxis: {
        valueFormatter: ['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4'],
        granularityEnabled: true,
        granularity: 1

      }

    };
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null})
    } else {
      this.setState({...this.state, selectedEntry: entry})
    }

    console.log(event.nativeEvent)
  }

  render() {
    const { selectedEntry } = this.state
    return (
      <View>
        <View center style={{height: HEIGHT / 2 }}>
          <View
            unflex
            style={{
              height: 60,
              backgroundColor: 'rgb(153,62,155)',
              padding: 10,
              width: WIDTH,
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontSize: 30,
                color: '#f0f0f0',
                fontWeight: 'bold',
              }}
            >Detail</Text>
          </View>
          <View
            unflex
            style={{
              height: 60,
              backgroundColor: 'rgb(153,62,155)',
              padding: 10,
              width: WIDTH,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                color: '#f0f0f0',
                fontWeight: 'bold',
                marginBottom: 0,
              }}
            >Registrasi {selectedEntry && selectedEntry.data.marker[selectedEntry.x]}</Text>
          </View>
          <View
            unflex
            style={{
              height: 60,
              backgroundColor: 'rgb(153,62,155)',
              padding: 10,
              width: WIDTH,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                color: '#f0f0f0',
                fontWeight: 'bold',
                marginBottom: 0,
              }}
            >Jumlah Lender {selectedEntry && selectedEntry.data.y[selectedEntry.x]}</Text>
          </View>
        </View>
        <View>
          <BarChart
            style={{ flex: 1 }}
            xAxis={this.state.xAxis}
            data={this.state.data}
            legend={this.state.legend}
            drawValueAboveBar={false}
            marker={{
              enabled: true,
              markerColor: processColor('rgb(153,62,155)'),
              textColor: processColor('white'),
              markerFontSize: 14,
            }}
            highlights={this.state.highlights}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      </View>
    );
  }
}
