import React from 'react'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer, createSwitchNavigator, createBottomTabNavigator, createStackNavigator, StackNavigator } from 'react-navigation'
import Login from './login'
import Friends from './friends'
import Statistics from './statistics'
import Account from './account'
import General from './statistics/general'
import Specific from './statistics/specific'
import AddFriends from './friends/addfriend'
import InvitedLists from './friends/invitedlist'

const TabMainMenu = createBottomTabNavigator(
  {
    Friends: Friends,
    Statistics: Statistics,
    Account: Account,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Friends') {
          iconName = `ios-contacts`;
        } else if (routeName === 'Statistics') {
          iconName = `ios-stats`;
        } else if (routeName === 'Account') {
          iconName = `ios-contact`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'rgb(255,223,0)',
      inactiveTintColor: 'rgb(240,240,240)',
      tabStyle: {
        height: 50,
      },
      allowFontScaling: true,
      style: {
        backgroundColor: 'rgb(153,62,155)', //rgb(21,66,118)',
      },
      labelStyle: {
        fontSize: 14,
      },
    },
    animationEnabled: true,
  }
)

const App = createStackNavigator(
  {
    TabMainMenu: TabMainMenu,
    General: { screen: General },
    Specific: { screen: Specific },
    AddFriends: { screen: AddFriends },
    InvitedLists: { screen: InvitedLists },

  },
  {
    initialRouteName: 'TabMainMenu',
    headerMode: 'none',
  }
)

const AppNavigator = createSwitchNavigator(
  {
    Login: Login,
    App: App,
  },
  {
    initialRouteName: 'App',
  },
)

const AppContainer = createAppContainer(AppNavigator);

// Now AppContainer is the main component for React to render

export default AppContainer;