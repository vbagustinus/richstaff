import React, { Component } from 'react';
import { Text, Image, AsyncStorage, Clipboard, ToastAndroid } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import { View, TouchableOpacity } from '../components'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin'

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDatas: null,
    };
    this.signOut = this.signOut.bind(this)
    this.copyLink = this.copyLink.bind(this)
  }

  async componentDidMount() {
    const { navigation } = this.props
    try {
      const value = await AsyncStorage.getItem('user');
      if (value !== null) {
        // We have data!!
        console.log(value);
        this.setState({
          userDatas: JSON.parse(value),
        })
      } else {
        await AsyncStorage.removeItem('user')
        .then(async () => {
          const navigateAction = NavigationActions.navigate({
            routeName: 'AppNavigator',
            action: NavigationActions.navigate({ routeName: 'Login' }),
          });
          
          this.props.navigation.dispatch(navigateAction);
          console.log('MASUK KOSONG')
        })
      }
     } catch (error) {
       // Error retrieving data
     }
  }

  signOut = async () => {
    try {
      // await GoogleSignin.revokeAccess();
      // await GoogleSignin.signOut();
      await AsyncStorage.removeItem('user')
        .then(async () => {
          this.setState({ userDatas: null });
          this.props.navigation.navigate('Login');
          console.log('MASUK KOSONG')
        })
    } catch (error) {
      console.error(error);
    }
  };

  copyLink = async () => {
    const { userDatas } = this.state
    await Clipboard.setString(userDatas && userDatas.code.referral);
    ToastAndroid.show('Berhasil di copy!', ToastAndroid.SHORT);
  }

  render() {
    const { userDatas } = this.state
    return (
      <View centering>
        <Image
          style={{width: 100, height: 100, borderRadius: 50, margin: 10}}
          source={{uri: userDatas && userDatas.user.photo }}
        />
        <Text
          style={{
            textAlign: 'center',
            fontSize: 20,
            color: 'rgb(153,62,155)',
            fontWeight: 'bold',
            margin: 10
          }}
        >
          { userDatas && userDatas.user.name }
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 15,
            color: 'rgb(153,62,155)',
            fontWeight: 'bold',
            fontStyle: 'italic',
            margin: 10,
          }}
        >
          { userDatas && userDatas.user.email }
        </Text>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 15,
            color: 'rgb(153,62,155)',
            fontWeight: 'bold',
            fontStyle: 'italic',
            margin: 10,
          }}
        >
          Kode Referal: { userDatas && userDatas.code.referral }
        </Text>
        <TouchableOpacity
          centering
          unflex
          onPress={this.copyLink}
          style={{
            height: 20,
            width: 100,
            margin: 10,
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              fontSize: 10,
              color: 'rgb(153,62,155)',
            }}
          >
            Copy link
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          centering
          unflex
          onPress={this.signOut}
          style={{
            height: 40,
            width: 200,
            margin: 10,
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              color: 'rgb(153,62,155)',
            }}
          >
            Keluar
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
