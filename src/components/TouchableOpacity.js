/* eslint-disable react/destructuring-assignment, react/prop-types */
import React from 'react'
import { TouchableOpacity as DefaultTouchableOpacity, StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  defaultStyle: {
    flex: 1,
    borderRadius: 30,
    borderBottomWidth: 0,
    shadowColor: 'purple',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 30,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    backgroundColor: 'transparent',
  },
  unflex: {
    flex: 0,
  },
  row: {
    flexDirection: 'row',
  },
  centering: {
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const TouchableOpacity = props => {
  const { defaultStyle, unflex, row, centering } = Styles
  return (
    <DefaultTouchableOpacity
      onPress={props.onPress}
      style={[
        defaultStyle,
        props.unflex && unflex,
        props.row && row,
        props.centering && centering,
        props.style,
      ]}
    >
      {props.children}
    </DefaultTouchableOpacity>
  )
}
