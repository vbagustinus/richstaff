/* eslint-disable react/destructuring-assignment, react/prop-types */
import React from 'react'
import ViewOverflow from 'react-native-view-overflow'
import { View as DefaultView, StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  defaultStyle: {
    flex: 1,
    fontSize: 12,
  },
  unflex: {
    flex: 0,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  centering: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    // justifyContent: 'center',
    alignItems: 'center',
  },
  spacebetween: {
    justifyContent: 'space-between',
  },
  right: {
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
  },
  left: {
    justifyContent: 'flex-start',
    alignContent: 'flex-start',
  },
  absolute: {
    position: 'absolute',
  },
})

export const View = props => {
  const {
    defaultStyle,
    unflex,
    row,
    centering,
    left,
    spacebetween,
    right,
    absolute,
    center,
  } = Styles
  return props.overflow ? (
    <ViewOverflow
      style={[
        defaultStyle,
        props.unflex && unflex,
        props.row && row,
        props.centering && centering,
        props.center && center,
        props.spacebetween && spacebetween,
        props.left && left,
        props.right && right,
        props.absolute && absolute,
        props.style,
      ]}
    >
      {props.children}
    </ViewOverflow>
  ) : (
    <DefaultView
      style={[
        defaultStyle,
        props.unflex && unflex,
        props.row && row,
        props.centering && centering,
        props.center && center,
        props.spacebetween && spacebetween,
        props.left && left,
        props.right && right,
        props.absolute && absolute,
        props.style,
      ]}
    >
      {props.children}
    </DefaultView>
  )
}
