import React, { Component } from 'react';
import { Text, AsyncStorage, StyleSheet, FlatList, ScrollView  } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin'
import ActionButton from 'react-native-action-button';
import A from 'axios'
import { API } from '../api'
import Icon from 'react-native-vector-icons/Ionicons';
import { View } from '../components'

export default class Friends extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataUsers: [],
    };
  }
  

  async componentDidMount() {
    const { navigation } = this.props
    try {
      const value = await AsyncStorage.getItem('user');
      if (value === null) {
        await AsyncStorage.removeItem('user')
        .then(async () => {
          await this.props.navigation.navigate('Login');
        })
      } else {
        const user = JSON.parse(value)
        console.log(user);
        A.get(`${API}friends/${user && user.accessToken}`)
        .then(({ data }) => {
          this.setState({ dataUsers: data.data })
        })
        .catch(err => {
          console.log(err)
        })
      }
     } catch (error) {
       console.log(error)
       // Error retrieving data
     }
  }

  _keyExtractor = (item, index) => item.id;

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

  _renderItem = ({item}) => (
    <View
      row
      center
      style={{
        height: 60,
        borderTopColor: 'rgba(255,255,255, .5)',
        borderTopWidth: 1,
        padding: 10,
        paddingLeft: 30,
      }}
    >
      <View>
        <Text
          style={{
            fontSize: 15,
            color: '#f0f0f0',
            fontWeight: 'bold',
          }}
        >{item.email}</Text>
      </View>
      <Icon name="ios-send" style={[styles.actionButtonIcon, {marginHorizontal: 20}]} />
      <Icon name="ios-trash" style={[styles.actionButtonIcon, {marginHorizontal: 20}]} />
    </View>
  );

  render() {
    const { navigation: { navigate } } = this.props
    const { dataUsers } = this.state
    return (
      <View
        style={{
          backgroundColor: 'rgb(199,151,201)',
        }}
      >
        <View
          style={{
            backgroundColor: 'rgb(199,151,201)', 
            marginVertical: 30,
            marginBottom: 70,
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              fontSize: 20,
              color: '#f0f0f0',
              margin: 30,
              fontWeight: 'bold',
              marginVertical: 10,
              marginBottom: 50,
            }}
          >
            Your Friends
          </Text>
          <ScrollView>
            <FlatList
              data={dataUsers}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </ScrollView>
        </View>
        <ActionButton buttonColor="rgb(153,62,155)">
          <ActionButton.Item
            buttonColor='rgb(153,62,120)'
            title="New Guest"
            onPress={() => navigate('AddFriends')}
          >
            <Icon name="md-create" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor='rgb(153,62,120)'
            title="Invited Lists"
            onPress={() => navigate('InvitedLists')}
          >
            <Icon name="md-done-all" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
});