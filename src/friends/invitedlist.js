import React, { Component } from 'react';
import { Text, processColor, FlatList, ScrollView } from 'react-native';
import { PieChart } from 'react-native-charts-wrapper'
import A from 'axios'
import { API } from '../api'
import { View } from '../components'

export default class General extends Component {
  constructor(props) {
    super(props);
    const { state } = props.navigation
    this.state = {
      dataUsers: [],
    };
  }

  componentDidMount() {
    A.get('https://jsonplaceholder.typicode.com/users')
    .then(({ data }) => {
      this.setState({ dataUsers: data })
    })
    .catch(err => {
      console.log(err)
    })
  }

  _keyExtractor = (item, index) => item.id;

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null})
    } else {
      this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
    }

    console.log(event.nativeEvent)
  }

  _renderItem = ({item}) => (
    <View
      unflex
      style={{
        height: 60,
        backgroundColor: 'rgb(153,62,155)',
        padding: 10,
        paddingLeft: 30,
      }}
    >
      <Text
        style={{
          fontSize: 15,
          color: '#f0f0f0',
          fontWeight: 'bold',
          marginBottom: 0,
        }}
      >{item.name}</Text>
    </View>
  );

  render() {
    const { dataUsers, data } = this.state
    return (
      <View
        column
        style={{
          backgroundColor: 'rgb(199,151,201)',
        }}
      >
        <View style={{ backgroundColor: 'rgb(153,62,155)' }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 20,
              color: '#f0f0f0',
              margin: 30,
              fontWeight: 'bold',
              marginVertical: 10,
            }}
          >
            Invited History
          </Text>
          <ScrollView>
            <FlatList
              data={dataUsers}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}
