import React, { Component } from 'react';
import { Text, Image, AsyncStorage, Clipboard, ToastAndroid, TextInput, Dimensions } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import { View, TouchableOpacity } from '../components'
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin'
const { width: WIDTH, height: HEIGHT } = Dimensions.get('window')
export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userDatas: null,
    };
  }

  async componentDidMount() {
    const { navigation } = this.props
  }

  render() {
    const { userDatas } = this.state
    return (
      <View centering>
        <TextInput
          style={{height: 50, borderRadius: 4, borderWidth: 1, width: WIDTH - 100, margin: 5, borderColor: 'rgb(153,62,155)' }}
          onChangeText={(nama) => this.setState({nama})}
          value={this.state.nama}
          placeholder={'Nama'}
        />
        <TextInput
          style={{height: 50, borderRadius: 4, borderWidth: 1, width: WIDTH - 100, margin: 5, borderColor: 'rgb(153,62,155)' }}
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
          placeholder={'Email'}
        />
        <TextInput
          style={{height: 50, borderRadius: 4, borderWidth: 1, width: WIDTH - 100, margin: 5, borderColor: 'rgb(153,62,155)' }}
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
          placeholder={'Telephone'}
        />
        <TouchableOpacity
          centering
          unflex
          onPress={this.signOut}
          style={{
            height: 40,
            width: 200,
            margin: 10,
            marginTop: 50,
          }}
        >
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              color: 'rgb(153,62,155)',
            }}
          >
            Save
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
